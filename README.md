#Screenshot

#简介
截屏助手是一款截屏软件，体积小、功能好，方便快捷，是千万用户的截屏必备软件！注意首次使用前需root权限！

#演示
http://www.wandoujia.com/apps/com.mj.screenshot

#捐赠
开源，我们是认真的，感谢您对我们开源力量的鼓励。


![支付宝](https://git.oschina.net/uploads/images/2017/0607/164544_ef822cc0_395618.png "感谢您对我们开源力量的鼓励")
![微信](https://git.oschina.net/uploads/images/2017/0607/164843_878b9b7f_395618.png "感谢您对我们开源力量的鼓励")