package com.mj.screenshot.widget;

import com.mj.screenshot.R;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class DesktopLayout extends LinearLayout {

	public DesktopLayout(Context context) {
		super(context);
		setOrientation(LinearLayout.HORIZONTAL);
		LayoutParams mLayoutParams = new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		setLayoutParams(mLayoutParams);

		// 显示的ICON
		ImageView mImageView = new ImageView(context);
		mImageView.setImageResource(R.drawable.logo);
		addView(mImageView, mLayoutParams);
	}
}
